import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cen_tools",
    version="0.0.1",
    author="Cansu Dincer, Sumana Sharma, Paula Weidemüller, Gavin J Wright, Evangelia Petsalaki",
    author_email="cansu@ebi.ac.uk",
    description="CEN-tools",
    long_description=,
    long_description_content_type="text/markdown",
    url="https://gitlab.ebi.ac.uk/cansu/CEN_tools/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
