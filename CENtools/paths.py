import os

# Specify the paths to import them from the modules
path = os.getcwd() + "/venv/"
raw_data_path = path + "data/raw_data/"
curated_data_path = path + "data/curated_data/"
object_path = path + "data/objects/"
