"""
                                    ---------------------
                                     C E N  -  T O O L S

                                       CEN Construction
                                    ---------------------
"""


########################################################################################################################
########################################################################################################################
# IMPORT #

from paths import path, object_path, curated_data_path
if "CENs" not in os.listdir(path): os.system("mkdir %s" %path + "CENs")
cen_path = path + "CENs/"

from data_preparation.objects import Gene, Sanger, Broad, Integrated
from data_preparation.objects import deserialisation_project, deserialisation_gene
from data_preparation.curation import essentiality, expression
from data_preparation.curation import BEG, BNEG

import pandas, numpy, pickle, os, sklearn, networkx
from scipy import stats
from scipy.stats import mannwhitneyu


########################################################################################################################
########################################################################################################################
# INPUT #

essentiality_limitation, expression_limitation, correlation_limitation = None, None, 0.5
hotspot_genes = set(pandas.read_csv(data_path + "DATA/Genes_with_cosmic_hotspot_data.csv")["x"].values)
oncogenic_genes = set(pandas.read_csv(data_path + "DATA/Genes_with_sanger_oncogenic_data.csv")["x"].values)


########################################################################################################################
########################################################################################################################
# FUNCTIONS  #

def separate_CLs(project_object, site_property, site = None, mutation = None, gene = None,
                 hotspot = False, oncogenic = False):
    """
    Separation of Cell lines according to their tissue of origin
    :param project_object: BROAD or SANGER (the project that is used)
    :param site: Interested tissue
    :param site_property: If site is a "tissue" name, "cancer" name or "cancer_subtype".
    :param mutation: The mutation that we need to separate the cell lines in accordance.
    :param hotspot: This variable can be used only with the gene and mutation variable.
                    If True only the hotspot mutation or hotspot carrying genes are taking into account!
    :param gene: The gene having mutation that we need to separate the cell lines in accordance.
    :return: The cell line dictionary for interested site and cell lines, cell line dictionary for
             interested site (or all) but separated inside the group such as by mutation
    """

    grouped_CLs, inside_grouped_CLs = {}, {}
    all_CLs, normal_CLs = [],[]

    for i, l in project_object.items():

        project_site = l.tissue if site_property =="tissue" else(
            l.cancer if site_property == "cancer" and l.cancer != None else(
                l.cancer_subtype if site_property == "cancer_subtype" and l.cancer_subtype != None else None))

        if project_site != False and project_site != None and type(project_site) != float:

            if project_site.upper() != "NORMAL":

                if i not in all_CLs:
                    all_CLs.append(i)

                if project_site.upper() not in grouped_CLs.keys():
                    grouped_CLs[project_site.upper()] = [i]

                else:
                    grouped_CLs[project_site.upper()].append(i)

            else:

                if i not in normal_CLs:
                    normal_CLs.append(i)

    if site != None:
        CLs = grouped_CLs[site.upper()]
        non_CLs = [CL for CL in all_CLs if CL not in CLs]

        if gene != None:

            # We will focus on only CLs because there is site and mutated gene are specified!
            if hotspot == False and oncogenic == True:
                mutated_CLs = [cl for cl in CLs if project_object[cl].oncogenic_mutated_genes != None and
                               gene in project_object[cl].oncogenic_mutated_genes]
                non_mutated_CLs = [cl for cl in CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(gene + " Mutated").upper(): mutated_CLs, "WT": non_mutated_CLs}

            elif hotspot == True and oncogenic == False:

                mutation_info_CLs = [cl for cl in CLs if project_object[cl].hotspot_mutations != None]

                mutated_CLs = [cl for cl in mutation_info_CLs if gene in
                               set([m.split(" p.")[0] for m in project_object[cl].hotspot_mutations])]

                non_mutated_CLs = [cl for cl in CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(gene + " HS-Mutated").upper(): mutated_CLs, "WT": non_mutated_CLs}

        elif mutation != None:

            # We will focus in CLs because there is site and mutation are specified!

            if hotspot == False and oncogenic == True:
                mutated_CLs = [cl for cl in CLs if project_object[cl].oncogenic_mutations != None and
                               mutation in project_object[cl].oncogenic_mutations]
                non_mutated_CLs = [cl for cl in CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(mutation).upper(): mutated_CLs, "WT": non_mutated_CLs}

            elif hotspot == True and oncogenic == False:

                mutation_info_CLs = [cl for cl in CLs if project_object[cl].hotspot_mutations != None]

                mutated_CLs = [cl for cl in mutation_info_CLs if mutation in project_object[cl].hotspot_mutations]
                non_mutated_CLs = [cl for cl in CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {("HS-"+mutation).upper(): mutated_CLs, "WT": non_mutated_CLs}

    else:

        if gene != None:

            if hotspot == False and oncogenic == True:

                mutated_CLs = [cl for cl in all_CLs if project_object[cl].mutated_genes != None and
                               gene in project_object[cl].mutated_genes]
                non_mutated_CLs = [cl for cl in all_CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(gene + " Mutated").upper(): mutated_CLs, "WT": non_mutated_CLs}

            elif hotspot == True and oncogenic == False:

                mutation_info_CLs = [cl for cl in all_CLs if project_object[cl].hotspot_mutations != None]

                mutated_CLs = [cl for cl in mutation_info_CLs if gene in
                               set([m.split(" p.")[0] for m in project_object[cl].hotspot_mutations])]
                non_mutated_CLs = [cl for cl in all_CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(gene + " HS-Mutated").upper(): mutated_CLs, "WT": non_mutated_CLs}

        elif mutation != None:

            # We will focus in CLs because there is site and mutation are specified!

            if hotspot == False and oncogenic == True:
                mutated_CLs = [cl for cl in all_CLs if project_object[cl].mutations != None and
                               mutation in project_object[cl].mutations]
                non_mutated_CLs = [cl for cl in all_CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {(mutation).upper(): mutated_CLs, "WT": non_mutated_CLs}

            elif hotspot == True and oncogenic == False:

                mutation_info_CLs = [cl for cl in all_CLs if project_object[cl].hotspot_mutations != None]

                mutated_CLs = [cl for cl in mutation_info_CLs if mutation in project_object[cl].hotspot_mutations]

                non_mutated_CLs = [cl for cl in all_CLs if cl not in mutated_CLs]

                inside_grouped_CLs = {("HS-"+mutation).upper(): mutated_CLs, "WT": non_mutated_CLs}


    return grouped_CLs, inside_grouped_CLs


def difference_between_multiple_groups(grouped_CL_dict, df, gene_y, gene_x, x_by, y_by,
                                       group_by, searched_context, limitation=None):
    """
    Kruskal-Wallis Test between multiple categorical and 1 continuous groups
    :param grouped_CL_dict: The dictionary of group of cell lines in different tissue/cancer/cancer subtype.
    :param df: DataFrame having values for interested property.
    :param gene_y: Intersted Gene that are measured.
    :param gene_x: The gene change the classification of cell lines or same as gene_y.
    :param x_by : Categorical classification having multiple groups such as "TISSUE", "CANCER", "CANCER SUBTYPE".
    :param y_by : Measured (continuous) property such as "ESSENTIALTIY", "CNVs", "EXPRESSION" etc.
    :param group_by : How we are separating the group according to "TISSUE" or "CANCER", "CANCER SUBTYPE".
    :param searched_context : The context we are searching on.
    :param limitation: The float value represents the minimum threshold of median of significantly selected group.
    :return: significance (boolean value if the result significant or not), p (p value of the test),
            group_value_df (Data frame for values and corresponding tissue/cancer/cancer subtype label,
            significant_groups (The groups having significantly higher value for the interested property.)
    """

    significant_groups = {}
    group_value_df = None

    # LIST OF VALUES

    if gene_x in list(df.index) and gene_y in list(df.index):
        value_dfs = []
        for group, cl_list in grouped_CL_dict.items():
            cl_values = list(df.loc[gene_y, [cl for cl in cl_list if cl in df.columns]])

            if len(cl_values) >= 3:
                # print("The cell line number is enough for %s: %s" %(group, str(len(cl_values))))
                value_dfs.append(pandas.DataFrame({group: cl_values}))
            else:
                """
                print("The cell line number is NOT enough for %s: %s" % (group, str(len(cl_values))))
                """
        group_value_df = None
        if value_dfs != []:
            group_value_df = pandas.melt(pandas.concat(value_dfs, axis=1))
            group_value_df = group_value_df.dropna()
            group_value_df.columns = ["Group", "Value"]

            # KRUSKAL-WALLIS TEST BETWEEN GROUPS

            if len(set(list(group_value_df.Value))) != 1:

                _, kruskal_p_val = stats.kruskal(*[list(i[1].Value) for i in group_value_df.groupby(["Group"])])

                if kruskal_p_val < 0.05:

                    # NOT POST-HOC BUT ONE BY ONE MANN WHITNEY U TEST

                    for group in set(group_value_df.Group):
                        data = [list(group_value_df[group_value_df.Group == group].Value),list(group_value_df[group_value_df.Group != group].Value)]

                        if sum(data[0]) != 0 and sum(data[1]) != 0:
                            _, mann_p_val = mannwhitneyu(data[0], data[1])

                            if mann_p_val < 0.05:

                                if limitation != None and numpy.median(data[0]) >= limitation:

                                    significant_groups[group] = {"MWU_P": mann_p_val, "DATA": data}

                                elif limitation == None:

                                    significant_groups[group] = {"MWU_P": mann_p_val, "DATA": data}

    return group_value_df, significant_groups


def difference_between_groups(grouped_CL_dict, df, gene_y, gene_x, x_by, y_by, group,
                              group_by, searched_context, limitation = None):
    """
    Mann Whitney Test between 1 categorical and 1 continuous groups
    :param grouped_CL_dict: The dictionary of group of cell lines in different tissue/cancer/cancer subtype.
    :param df: DataFrame having values for interested property.
    :param gene_y: Intersted Gene that are measured.
    :param gene_x: The gene change the classification of cell lines or same as gene_y.
    :param x_by : Categorical classification having multiple groups such as "TISSUE", "CANCER", "CANCER SUBTYPE".
    :param y_by : Measured (continuous) property such as "ESSENTIALTIY", "CNVs", "EXPRESSION" etc.
    :param group : The name of the interested tissue/cancer/cancer subtype or name of the gene mutated or mutation.
    :param group_by : How we are separating the group according to "TISSUE" or "CANCER", "CANCER SUBTYPE".
    :param searched_context : The context we are searching on.
    :param limitation: The float value represents the minimum threshold of median of interested group
    :return: significance (boolean value if the result significant or not}, p (p value of the test
    """

    # SITE

    group = group.upper()
    significance, mann_p_val, data = None, None, None

    # LIST OF VALUES

    if gene_x in list(df.index) and gene_y in list(df.index):

        cl_values = list(df.loc[gene_y, [CL for CL in grouped_CL_dict[group] if CL in df.columns]])
        other_cl_values = list(df.loc[gene_y, [CL for CL in grouped_CL_dict["WT"] if CL in df.columns]])

        n = 0
        group_value_df = pandas.DataFrame(0, index=range(len(cl_values) + len(other_cl_values)),
                                          columns=["Group", "Value"])
        if len(cl_values) >= 3:
            #print("The cell line number is enough for %s: %s" %(group, str(cl_values)))
            for k in cl_values:
                group_value_df.loc[n, "Group"] = group
                group_value_df.loc[n, "Value"] = k
                n += 1
            for k in other_cl_values:
                group_value_df.loc[n, "Group"] = "OTHER"
                group_value_df.loc[n, "Value"] = k
                n += 1

        # MANN WHITNEY U TEST BETWEEN TWO GROUPS

        data = [list(group_value_df[group_value_df.Group == group].Value),
                list(group_value_df[group_value_df.Group == "OTHER"].Value)]

        if len(data[0]) >= 3 and data[0] != data[1]:

            _, mann_p_val = mannwhitneyu(data[0], data[1])

            if mann_p_val < 0.05:

                if limitation != None and numpy.median(data[0]) >= limitation: significance = True

                elif limitation == None: significance = True

    return significance, mann_p_val, data


def association_between_groups(grouped_CL_dict, gene_x, gene_y, df_x, df_y, site,
                               group_by, searched_context, limitation=None, x_by=None, y_by=None):
    """
    Correlation and Regression analysis between 2 continuous groups
    :param grouped_CL_dict: The dictionary of group of cell lines in different tissue/cancer/cancer subtype.
    :param gene_x : The gene whose information will be taken from df_x
    :param gene_y : The gene whose information will be taken from df_y
    :param df_x : LMPlot x axis property (if essentiality will be used, use in x axis)
    :param df_y : LMPlot y axis property (if expression will be used, use in y axis)
    :param x_by : Which property will be represented "EXPRESSION", "ESSENTIALITY", "CNVs" in df_x
    :param y_by : Which property will be represented "EXPRESSION", "ESSENTIALITY", "CNVs" in df_y
    :param site : The name of the interested tissue/cancer/cancer subtype.
    :param group_by : How we are separating the group according to "TISSUE" or "MUTATION"
    :param searched_context : The context we are searching on
    :param limitation: The float value represents the minimum threshold of median of interested group
    :return: True or None for association, df which is concatanated dataframe or None , cors or None, titles or None
    """

    # SITE

    association, cor_coef, cor_p, df = None, None, None, None

    # Property matrix do not always have all the cell lines in the essentiality matrix.
    # Take intersection
    """
    if group_by != None:

        association = None

        if gene_x in list(df_x.index) and gene_y in list(df_y.index):

            df = pandas.concat([pandas.DataFrame(df_x.loc[gene_x, cls]),
                                pandas.DataFrame(df_y.loc[gene_y, cls])], axis = 1)
            df = df.dropna(axis = 1, how = "all")


            df[group_by] = site
            if len(df.columns) == 3:
                df.columns = [x_by, y_by, group_by]

            other_df = pandas.concat([pandas.DataFrame(df_x.loc[gene_x, new_other_CLs]),
                                      pandas.DataFrame(df_y.loc[gene_y, new_other_CLs])], axis = 1)
            other_df = other_df.dropna(axis = 1, how = "all")
            other_df[group_by] = non_interested_group
            if len(other_df.columns) == 3:
                other_df.columns = [x_by, y_by, group_by]
    """

    if site != None:

        cls = [cl for cl in grouped_CL_dict[site.upper()] if cl in df_x.columns and cl in df_y.columns]

    else:
        cls = list(set(df_x.columns).intersection(set(df_y.columns)))

    if len(cls) >= 5:

        if gene_x in list(df_x.index) and gene_y in list(df_y.index):

            if type(df_y.loc[gene_y]) == pandas.Series and type(df_x.loc[gene_x]) == pandas.Series:

                df = pandas.concat([pandas.DataFrame(df_x.loc[gene_x, cls]),
                                    pandas.DataFrame(df_y.loc[gene_y, cls])], axis=1)

                df = df.dropna()

                if len(df.columns) == 2 and len(df.index) >= 5:
                    df.columns = [x_by, y_by]

                    # Correlation between properties

                    cor_coef, cor_p = stats.pearsonr(df[x_by], df[y_by])

                    if limitation != None and numpy.abs(cor_coef) >= limitation: association = True

    return association, cor_coef, cor_p, df


def store_networkx_object(network_obj, context_name, project, efct= None):

    global network_path

    network_df = pandas.DataFrame(0, index=list(range(len(network_obj.edges()))),
                                  columns=["from", "to", "pvalue", "median_group", "median_other",
                                           "effector", "affected", "group_cl", "other_cl",
                                           "tissue", "association", "to_Who", "group", "project"])
    i = 0
    for edge in network_obj.edges:
        network_df.loc[i, "from"] = edge[0]
        network_df.loc[i, "to"] = edge[1]
        network_df.loc[i, "pvalue"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["p_value"]
        network_df.loc[i, "median_group"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["median_group"]
        network_df.loc[i, "median_other"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["median_other"]
        network_df.loc[i, "effector"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["effector"]
        network_df.loc[i, "affected"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["affected"]
        network_df.loc[i, "group_cl"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["group_cl"]
        network_df.loc[i, "other_cl"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["other_cl"]
        network_df.loc[i, "tissue"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["tissue"]
        network_df.loc[i, "association"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["association"]
        network_df.loc[i, "to_Who"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["to_who"]
        network_df.loc[i, 'group'] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["group"]
        network_df.loc[i, "project"] = network_obj.get_edge_data(edge[0], edge[1], edge[2])["project"]
        i += 1


    if efct is None: network_df.to_csv(network_path + project + "_" +  context_name + "_CEN.csv")
    else: network_df.to_csv(network_path + project + "_" + efct + "_CEN.csv")

    return 1


# EXPRESSION #

"""
EDGES:
    1. Query gene expression is significantly high in a cancer (linkeage)
    2. Query gene essentiality is significantly high in a cancer (linkeage)
    3. Query gene expression associated with query gene essentiality (all cancers / not separate)
"""

def expression_analysis(project, ess_df, project_obj, exp_df, tissue_cancer):
    expression_network = networkx.MultiDiGraph()
    gene_number = 0
    for gene in list(ess_df.index):
        print(gene)
        print(str(100 * gene_number / float(len(list(ess_df.index)))))
        print()
        gene_number +=1

        grouped_CLs, _ = separate_CLs(project_object=project_obj, site_property=tissue_cancer)

        # Multiple group difference / EDGE 1

        group_value_df, significancy_dict = difference_between_multiple_groups(
            grouped_CL_dict=grouped_CLs, df=exp_df, gene_y=gene, gene_x=gene,
            x_by=tissue_cancer.upper(), y_by="EXPRESSION", group_by=tissue_cancer.upper(),
            searched_context=tissue_cancer.upper(), limitation=expression_limitation)

        for significant_cancer, d in significancy_dict.items():
            # NETWORK EDGE (LINKAGE)

            expression_network.add_edge(gene, significant_cancer, p_value=d["MWU_P"],
                                        median_group=numpy.median(d["DATA"][0]),
                                        median_other=numpy.median(d["DATA"][1]),
                                        effector="cancer", affected="expression", tissue=significant_cancer,
                                        group_cl=len(d["DATA"][0]), other_cl=len(d["DATA"][1]),
                                        to_who="PANCANCER", association=None, group=tissue_cancer,
                                        project=project)

        # Multiple group difference / EDGE 2

        group_value_df, significancy_dict = difference_between_multiple_groups(
            grouped_CL_dict=grouped_CLs, df=ess_df, gene_y=gene, gene_x=gene,
            x_by=tissue_cancer.upper(), y_by="ESSENTIALITY", group_by=tissue_cancer.upper(),
            searched_context=tissue_cancer.upper(), limitation=essentiality_limitation)

        for significant_cancer, d in significancy_dict.items():
            # NETWORK EDGE (LINKAGE)
            expression_network.add_edge(gene, significant_cancer, p_value=d["MWU_P"],
                                        median_group=numpy.median(d["DATA"][0]),
                                        median_other=numpy.median(d["DATA"][1]),
                                        effector=tissue_cancer, affected="essentiality", tissue=significant_cancer,
                                        group_cl=len(d["DATA"][0]), other_cl=len(d["DATA"][1]),
                                        to_who="PANCANCER", association=None, group=tissue_cancer, project=project)

        # Expression to Essentiality Association / EDGE 3

        exp_association, exp_cor_coef, exp_cor_p, exp_cor_df = association_between_groups(
            grouped_CL_dict=grouped_CLs, gene_x=gene, gene_y=gene, df_x=ess_df,
            df_y=exp_df, site=None, group_by=None, searched_context="EXPRESSION",
            limitation=correlation_limitation, x_by="ESSENTIALITY", y_by="EXPRESSION")

        if exp_association != None and exp_cor_p < 0.05:

            # NETWORK EDGE (EXP-ESS ASSOCIATION)
            expression_network.add_edge(gene, gene, p_value=exp_cor_p, median_group=None,
                                        median_other=None, effector="expression", affected="essentiality",
                                        association=exp_cor_coef,
                                        group_cl=len(list(exp_cor_df.index)), other_cl=None,
                                        to_who="PANCANCER", tissue="PANCANCER", group=tissue_cancer, project=project)

            for cancer in grouped_CLs.keys():

                # Expression to Essentiality Association in cancer type / EDGE 4

                cancer_exp_association, cancer_exp_cor_coef, cancer_exp_cor_p, cancer_exp_cor_df = association_between_groups(
                    grouped_CL_dict=grouped_CLs, gene_x=gene, gene_y=gene, df_x=ess_df,
                    df_y=exp_df, site=cancer, group_by=tissue_cancer, searched_context="EXPRESSION",
                    limitation=correlation_limitation, x_by="ESSENTIALITY", y_by="EXPRESSION")

                if cancer_exp_association != None and cancer_exp_cor_p < 0.05:
                    # NETWORK EDGE 4 (EXP-ESS ASSOCIATION IN TISSUE)

                    expression_network.add_edge(gene, gene, p_value=cancer_exp_cor_p, median_group=None,
                                                median_other=None, group_cl=len(list(cancer_exp_cor_df.index)),
                                                other_cl=None, effector="expression", affected="essentiality",
                                                association=cancer_exp_cor_coef, to_who=cancer, tissue=cancer,
                                                group=tissue_cancer, project=project)
    return expression_network


# MUTATION #

"""
EDGES:
    1. Query gene essentiality in mutated state is significantly higher than wild type (all cancers/tissues + separately)
    # ONLY FOR SANGER ONCOGENIC GENES AND CCLE HOTSPOT MUTATIONS 
    # If cell line has a hotspot/oncogenic mutation on the query gene it is associated with the hotspot/oncogenic mutation analysis 
"""

def mutation_analysis(project, ess_df, project_obj, tissue_cancer, hs_onco, efct):
    mutation_network = networkx.MultiDiGraph()
    gene_number = 0
    for gene in list(ess_df.index):
        print(gene)
        print(str(100*gene_number/float(len(list(ess_df.index)))))
        print()
        gene_number += 1

        if gene in hotspot_genes:
            # All cancers not a specific ones.
            grouped_CLs, inside_grouped_CLs = separate_CLs(
                project_object=project_obj, site_property=tissue_cancer, site=None,
                mutation=None, gene=gene, hotspot=True, oncogenic=False)

            # Check if mutated cell lines have a significantly higher essentiality than WT or not!

            mut_significance, mut_mann_p_val, mut_data = difference_between_groups(
                grouped_CL_dict=inside_grouped_CLs, df=ess_df, gene_y=gene, gene_x=gene,
                x_by="MUTATION", y_by="ESSENTIALITY", group=gene + " " + hs_onco.upper() + "-Mutated", group_by="MUTATION",
                searched_context="MUTATION", limitation=essentiality_limitation)

            if mut_significance:

                # NETWORK EDGE (MUTATION / EDGE 1)
                mutation_network.add_edge(
                    gene, gene, p_value=mut_mann_p_val, median_group=numpy.median(mut_data[0]),
                    median_other = numpy.median(mut_data[1]), effector=efct, affected="essentiality",
                    group_cl=len(mut_data[0]), other_cl=len(mut_data[1]), association=None,
                    to_who="WT " + gene, tissue="PANCANCER", group = tissue_cancer, project = project)

            # Cancers by cancers

            for cancer in grouped_CLs.keys():

                # Specific oncogenic mutated gene in specific cancer!

                cancer_grouped_CLs, inside_cancer_grouped_CLs = separate_CLs(
                    project_object=project_obj, site_property=tissue_cancer, site=cancer,
                    mutation=None, gene=gene, hotspot=True, oncogenic=False)

                # Check if mutated cell lines have a significantly higher essentiality than WT or not!

                mut_cancer_significance, mut_cancer_mann_p_val, mut_cancer_data = \
                    difference_between_groups(
                        grouped_CL_dict=inside_cancer_grouped_CLs, df=ess_df, gene_y=gene,
                        gene_x=gene, x_by="MUTATION", y_by="ESSENTIALITY", group=gene + " " + hs_onco.upper() + "-Mutated",
                        group_by="MUTATION", searched_context="MUTATION", limitation=essentiality_limitation)

                if mut_cancer_significance:

                    # NETWORK EDGE (MUTATION / EDGE 1)
                    mutation_network.add_edge(
                        gene, cancer, p_value=mut_cancer_mann_p_val, median_group=numpy.median(mut_cancer_data[0]),
                        median_other = numpy.median(mut_cancer_data[1]), effector=efct, affected="essentiality",
                        group_cl=len(mut_cancer_data[0]), other_cl=len(mut_cancer_data[1]), association=None,
                        to_who="WT " + gene, tissue=cancer, group = tissue_cancer, project = project)
    return mutation_network


# CO-MUTATION #

"""
EDGES:
    1. Another gene mutation affects query gene essentiality (all tissues/cancers)
"""

def co_mutation_analysis(project, ess_df, project_obj, tissue_cancer, hs_onco, efct):
    co_mutation_network = networkx.MultiDiGraph()
    gene_number = 0
    for gene in list(ess_df.index):
        for other_gene in list(ess_df.index):
            if other_gene != gene:
                print(gene + " - " + other_gene + " - " + project)
                print(str(100 * gene_number / float(len(ess_df.index))))
                print()
                gene_number += 1

                # All cancers not a specific ones.

                grouped_CLs, inside_grouped_CLs = separate_CLs(
                    project_object=project_obj, site_property=tissue_cancer, site=None,
                    mutation=None, gene=other_gene, hotspot=True, oncogenic=False)

                # Check if mutated cell lines have a significantly higher essentiality than WT or not!

                mut_significance, mut_mann_p_val, mut_data = difference_between_groups(
                    grouped_CL_dict=inside_grouped_CLs, df=ess_df, gene_y=gene, gene_x=other_gene,
                    x_by="MUTATION", y_by="ESSENTIALITY", group=other_gene+ " " + hs_onco.upper() + "-Mutated",
                    group_by="CO-MUTATION",searched_context="CO-MUTATION", limitation=essentiality_limitation)

                if mut_significance:
                    # NETWORK EDGE (CO-MUTATION / EDGE 1)
                    co_mutation_network.add_edge(
                        other_gene, gene, p_value=mut_mann_p_val, median_group=numpy.median(mut_data[0]),
                        median_other=numpy.median(mut_data[1]), effector=efct,
                        affected="essentiality",
                        group_cl=len(mut_data[0]), other_cl=len(mut_data[1]), association=None,
                        to_who="WT " + other_gene, tissue="PANCANCER", group=tissue_cancer, project=project)

                ############################****************************############################

                # cancer by cancer

                for cancer in grouped_CLs.keys():

                    # Specific oncogenic mutated gene in specific cancer!

                    cancer_grouped_CLs, inside_cancer_grouped_CLs = separate_CLs(
                        project_object=project_obj, site_property=tissue_cancer, site=cancer,
                        mutation=None, gene=other_gene, hotspot=True, oncogenic=False)

                    # Check if mutated cell lines have a significantly higher essentiality than WT or not!

                    mut_cancer_significance, mut_cancer_mann_p_val, mut_cancer_data = \
                        difference_between_groups(
                            grouped_CL_dict=inside_cancer_grouped_CLs, df=ess_df, gene_y=gene,
                            gene_x=other_gene, x_by="MUTATION", y_by="ESSENTIALITY",
                            group=other_gene + " " + hs_onco.upper() + "-Mutated", group_by="CO-MUTATION",
                            searched_context="CO-MUTATION", limitation=essentiality_limitation)

                    if mut_cancer_significance:
                        # NETWORK EDGE (CO-MUTATION / EDGE 1)
                        co_mutation_network.add_edge(
                            other_gene, gene, p_value=mut_cancer_mann_p_val,
                            median_group=numpy.median(mut_cancer_data[0]),
                            median_other=numpy.median(mut_cancer_data[1]),
                            effector=efct, affected="essentiality", association=None,
                            group_cl=len(mut_cancer_data[0]), other_cl=len(mut_cancer_data[1]),
                            to_who="WT " + other_gene, tissue=cancer, group=tissue_cancer, project=project)
    return co_mutation_network


########################################################################################################################
########################################################################################################################
# MAIN RUN  #

networks = {}

def main(context, project, tissue_cancer, hs_onco):

    context_name = context if context in ["mutation", "expression"] else ("co_mutation" if context == "comutation" else None)
    hs_onco_name = "hotspot" if hs_onco == "HS" else ("onco" if hs_onco == "ONCO" else None)
    efct = context_name + "_" + hs_onco_name if context_name in ["mutation", "co_mutation"] else None

    # Read Essentiality Data Frame
    ess = essentiality("SANGER", "MANUEL") if project == "sanger" else (
        essentiality("BROAD", "MANUEL") if project == "broad" else essentiality("INTEGRATED", "MANUEL"))

    # Read Expression Data Frame
    exp = expression("SANGER") if project == "sanger" else (
        expression("BROAD") if project == "broad" else expression("INTEGRATED"))

    # Read Object
    obj = pickle.load(open(data_path + "OBJECTS/NEW/SANGER_MANUEL_OBJECT.pkl", "rb")) if project == "sanger" else (
        pickle.load(open(data_path + "OBJECTS/NEW/BROAD_MANUEL_OBJECT.pkl", "rb")) if project == "broad" else
        pickle.load(open(data_path + "OBJECTS/NEW/INTEGRATED_MANUEL_OBJECT.pkl", "rb")))

    # Analysis ..
    ntw = expression_analysis(project= project, ess_df = ess, project_obj = obj, exp_df=exp, tissue_cancer=tissue_cancer) \
        if context == "expression" else (
        mutation_analysis(project= project, ess_df = ess, project_obj = obj, tissue_cancer=tissue_cancer,
                          hs_onco=hs_onco.upper(), efct= efct)
        if context == "mutation" else (
            co_mutation_analysis(project= project, ess_df = ess, project_obj = obj, tissue_cancer=tissue_cancer,
                                 hs_onco=hs_onco.upper(), efct = efct)
            if context == "comutation" else None))

    if ntw is not None: networks[project] = ntw

    print(project + " " + context + " CEN Construction Ends!\n")

    # Pickled Network Object!
    pickle.dump(networks, open(cen_path + context + "_CEN.p", "wb"))

    # Pickle to csv file
    store_networkx_object(network_obj=ntw, context=context, project=project.upper(), efct = efct)

    print("NETWORKS WERE CREATED.")

    return True


def take_CEN_input():
    parser = argparse.ArgumentParser()
    parser.add_argument('-context', '--context', dest='context',  type=str, help='The context whose CEN will be created!')
    parser.add_argument('-project', '--project', dest='project', type= str, help ='The project whose CEN will be created!',
                        choises = ["sanger", "broad", "integrated"])
    parser.add_argument('-subgroup', '--subgroup', dest = "subgroup", type = str, help ='The subgroup whose CEN will be created',
                        choises = ["tissue", "cancer"])
    parser.add_argument('-mut_type', '--mut_type', dest= "hs_onco", type = str, help='Hotspot or Oncogenic based mutation CEN',
                        choises = ["HS", "ONCO"])
    input = parser.parse_args()
    return input


args = take_CEN_input()

main(context=args.context, project=args.project, tissue_cancer=args.subgroup, hs_onco=args.hs_onco)

